import java.util.ArrayList;
import java.util.Scanner;

public class A1_8 {

     public static void main(String[] args) {
        long startTime = System.nanoTime();

        ArrayList<Integer> A = new ArrayList<Integer>();
        Scanner kb = new Scanner(System.in);
        for (int i = 0; i <= 3; i++) {
            A.add(kb.nextInt());
        }
        System.out.println("A = " + A);
        A.add(A.get(0));
        for (int i = 0; i < 2; i++) {
            A.set(i, A.get(3));
            A.set(3, A.get(4));
            A.set(4, A.get(3 - 2));
        }
        A.remove(A.get(4));
        System.out.println("reverseA = " + A);

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println((double) totalTime / 1000000000 + " seconds");
    }

}
