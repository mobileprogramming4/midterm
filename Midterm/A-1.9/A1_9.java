
import java.util.Scanner;

public class A1_9 {

    public static void main(String[] args) {
        long startTime = System.nanoTime();

        Scanner kb = new Scanner(System.in);
        String S = kb.next();
        int intS = Integer.parseInt(S);
        System.out.println(intS + " is " + ((Object) intS).getClass().getSimpleName());
        
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println((double) totalTime / 1000000000 + " seconds");
    }

}
